
package com.gps.gate.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public final class DateUtil {

    private DateUtil() {
    }


    /**
     * 根据给定的字符串如：yyyy-MM-dd HH:mm:ss，（必须是这种格式） 返回一个日期日期形式
     *
     * @param strDate 要抛析的字符串,且字符串的形式必须：2007-09-10 07:00:00
     * @return 将字符串抛析成日期的格式返回
     * @throws ParseException 解析 format 字段失败
     */
    public static java.util.Date getDateByStr(String strDate, String format) throws ParseException {
        assert strDate != null && format != null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.parse(strDate);
    }

    /**
     * 根据给定的日期，返回给定的字符串， 返回 字符串的形式是：yyyy-MM-dd HH:mm:ss
     *
     * @param date 要格式化的日期
     * @return 将日期格式化后返回的字符串，以这中格式返回：yyyy-MM-dd HH:mm:ss
     */
    public static String getStrByDate(Date date, String format) {
        assert date != null && format != null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

}
