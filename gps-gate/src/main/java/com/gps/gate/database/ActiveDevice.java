
package com.gps.gate.database;

import com.gps.common.model.Command;
import com.gps.gate.NetworkMessage;
import com.gps.gate.Protocol;
import io.netty.channel.Channel;

import java.net.SocketAddress;


public class ActiveDevice {
    private final String imei;
    private final Protocol protocol;
    private final Channel channel;
    private final SocketAddress remoteAddress;
    private String status;

    public ActiveDevice(String imei, Protocol protocol, Channel channel, SocketAddress remoteAddress) {
        this.imei = imei;
        this.protocol = protocol;
        this.channel = channel;
        this.remoteAddress = remoteAddress;
    }

    public Channel getChannel() {
        return channel;
    }

    public String getImei() {
        return imei;
    }

    public void sendCommand(Command command) {
        protocol.sendDataCommand(this, command);
    }

    public void write(Object message) {
        channel.writeAndFlush(new NetworkMessage(message, remoteAddress));
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
