package com.gps.gate;


import com.gps.common.model.Command;
import com.gps.gate.database.ActiveDevice;

import java.util.Collection;
import java.util.List;

public interface Protocol {

    String getName();

    Collection<TrackerServer> getServerList();

    Collection<String> getSupportedDataCommands();

    void sendDataCommand(ActiveDevice activeDevice, Command command);

}
