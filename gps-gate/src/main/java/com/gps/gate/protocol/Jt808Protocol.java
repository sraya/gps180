
package com.gps.gate.protocol;

import com.gps.common.model.Command;
import com.gps.gate.BaseProtocol;
import com.gps.gate.PipelineBuilder;
import com.gps.gate.TrackerServer;

public class Jt808Protocol extends BaseProtocol {

    public Jt808Protocol() {
        setSupportedDataCommands(
                Command.TYPE_ENGINE_STOP,
                Command.TYPE_ENGINE_RESUME);

        addServer(new TrackerServer(false, getName(), 6801) {
            @Override
            protected void addProtocolHandlers(PipelineBuilder pipeline) {
                pipeline.addLast(new Jt808FrameDecoder());
                pipeline.addLast(new Jt808ProtocolEncoder());
                pipeline.addLast(new Jt808ProtocolDecoder(Jt808Protocol.this));
            }
        });
    }

}
