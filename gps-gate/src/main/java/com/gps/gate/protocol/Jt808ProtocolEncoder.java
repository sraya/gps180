
package com.gps.gate.protocol;

import com.gps.common.model.Command;
import com.gps.gate.BaseProtocolEncoder;
import com.gps.gate.helper.DataConverter;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Jt808ProtocolEncoder extends BaseProtocolEncoder {

    @Override
    protected Object encodeCommand(Command command) {

        ByteBuf id = Unpooled.wrappedBuffer(
                DataConverter.parseHex(command.getImei()));
        try {
            ByteBuf data = Unpooled.buffer();
            byte[] time = DataConverter.parseHex(new SimpleDateFormat("yyMMddHHmmss").format(new Date()));

            switch (command.getType()) {
                case Command.TYPE_ENGINE_STOP:
                    data.writeByte(0x01);
                    data.writeBytes(time);
                    return Jt808ProtocolDecoder.formatMessage(Jt808ProtocolDecoder.MSG_OIL_CONTROL, id, data);
                case Command.TYPE_ENGINE_RESUME:
                    data.writeByte(0x00);
                    data.writeBytes(time);
                    return Jt808ProtocolDecoder.formatMessage(Jt808ProtocolDecoder.MSG_OIL_CONTROL, id, data);
                default:
                    return null;
            }
        } finally {
            id.release();
        }
    }

}
