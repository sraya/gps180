
package com.gps.gate.protocol;

import com.alibaba.fastjson.JSON;
import com.gps.common.helper.UnitsConverter;
import com.gps.common.model.Device;
import com.gps.common.model.Position;
import com.gps.gate.BaseHttpProtocolDecoder;
import com.gps.gate.Protocol;
import com.gps.gate.database.ActiveDevice;
import com.gps.gate.helper.DateUtil;
import com.gps.gate.model.HttpData;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;

import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class HttpProtocolDecoder extends BaseHttpProtocolDecoder {

    public HttpProtocolDecoder(Protocol protocol) {
        super(protocol);
    }

    /**
     * 解析单条数据
     *
     * @param channel
     * @param remoteAddress
     * @param httpData
     */
    protected Position decodeData(Channel channel, SocketAddress remoteAddress, HttpData httpData) {
        Position position = null;
        try {
            position = new Position(getProtocolName());
            position.setValid(true);
            position.setImei(httpData.getImei());
            position.setLongitude(httpData.getLng());
            position.setLatitude(httpData.getLat());
            position.setCourse(httpData.getOri());
            position.setTime(DateUtil.getDateByStr(httpData.getLocTime(), "yyyy-MM-dd'T'HH:mm:ss"));
            position.setSpeed(UnitsConverter.knotsFromKph((float) httpData.getSpd()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return position;
    }

    @Override
    protected Object decode(Channel channel, SocketAddress remoteAddress, Object msg) {
        List<Position> list = new ArrayList<>();
        try {
            if (msg instanceof FullHttpRequest) {
                FullHttpRequest request = (FullHttpRequest) msg;
                ByteBuf buffer = request.content();
                if (buffer == null) {
                    return null;
                }
                List<HttpData> httpData = JSON.parseArray(buffer.toString(StandardCharsets.UTF_8), HttpData.class);
                if (httpData == null) {
                    sendResponse(channel, HttpResponseStatus.OK);
                    return null;
                }
                list = httpData.stream()
                        .map(c -> decodeData(channel, remoteAddress, c))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
                if (list.size() > 0) {
                    ActiveDevice deviceSession = getDeviceSession(channel, remoteAddress, list.get(0).getImei());
                    connectionManager.updateDevice(list.get(0).getImei(), Device.STATUS_ONLINE);
                }
            }
            sendResponse(channel, HttpResponseStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
