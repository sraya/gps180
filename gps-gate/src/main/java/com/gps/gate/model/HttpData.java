package com.gps.gate.model;

import lombok.Data;

@Data
public class HttpData {
    private String imei;  // 设备号
    private double lat;    // 纬度
    private double lng;    // 经度
    private double ori; // 方向角
    private double spd; // 速度
    private String locTime;        // 定位时间
}
