
package com.gps.gate.handler.events;

import com.gps.common.model.Event;
import com.gps.common.model.Position;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import java.util.Map;


public abstract class BaseEventHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        try {
            if (msg instanceof Position) {
                Position position = ((Position) msg);
                Map<Event, Position> events = analyzePosition(position);
                if (events != null) {
                    ctx.fireChannelRead(events);
                }
            } else {
                super.channelRead(ctx, msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected abstract Map<Event, Position> analyzePosition(Position position);

}
