
package com.gps.gate.handler.events;

import com.gps.common.model.CommandResult;
import com.gps.common.model.Position;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import io.netty.channel.ChannelHandler;

@ChannelHandler.Sharable
public class CommandResultEventHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        try {
            if (msg instanceof Position) {
                Position position = ((Position) msg);
                if (position.getAttributes().get(Position.KEY_RESULT) != null) {        //存在结果数据
                    CommandResult commandResult = new CommandResult();
                    commandResult.setImei(position.getImei());
                    commandResult.setCommandFlowId(position.getInteger(Position.KEY_RESULT_CMDINX));        //序列号
                    commandResult.setResult(position.getBoolean(Position.KEY_RESULT));
                    ctx.fireChannelRead(commandResult);

                    position.getAttributes().remove(Position.KEY_RESULT);
                }
                ctx.fireChannelRead(position);
            } else {
                super.channelRead(ctx, msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
