
package com.gps.gate.handler;

import com.gps.common.model.Position;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public abstract class BaseDataHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        try {
            if (msg instanceof Position) {
                Position position = handlePosition((Position) msg);
                if (position != null) {
                    ctx.fireChannelRead(position);
                }
            } else {
                super.channelRead(ctx, msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected abstract Position handlePosition(Position position);

}
