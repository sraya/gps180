package com.gps.gate.trans;

import com.gps.common.model.BaseModel;

public interface PosTrans {
    void send(BaseModel model);
}
