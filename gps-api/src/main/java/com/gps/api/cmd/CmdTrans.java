package com.gps.api.cmd;

import com.gps.common.model.Command;

public interface CmdTrans {
    void send(Command command);
}
