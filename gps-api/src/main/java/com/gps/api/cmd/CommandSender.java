package com.gps.api.cmd;

import com.gps.common.cache.CacheKeys;
import com.gps.common.future.SyncFuture;
import com.gps.common.model.Command;
import com.gps.common.model.CommandType;
import com.gps.db.GpsRedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class CommandSender {
    @Autowired
    private CmdTrans cmdTrans;
    @Autowired
    private GpsRedisUtils gpsRedisUtils;

    private SyncFuture<List<CommandType>> future;

    public void sendCommand(Command command) {
        try {
            int expireTime = 86400; // 1Day
            if (command.isSync()) {
                expireTime = 60;
            }
            gpsRedisUtils.set(CacheKeys.getCommandKeys(command.getType(), command.getImei()), command, expireTime);
            sendByKafka(command);
//            sendByApi(command);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<CommandType> waitCmds() {
        try {
            future = new SyncFuture<>();
            return future.get(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void putCmds(List<CommandType> cmds) {
        if (future != null) {
            future.setResponse(cmds);
            future = null;
        }
    }

    private void sendByKafka(Command command) {
        cmdTrans.send(command);
    }


}
