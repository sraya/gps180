package com.gps.common;

/**
 * 常量类
 */
public class Constants {

    //命令唯一标识
    public static final String USER_ID = "userId";
    public static final String USER_PHONE = "phone";
    public static final String PLATFORM = "platform";

    public static final String COMMAND_LOG_ID = "command_log_id";
}
