package com.gps.run;

import com.gps.gate.ServerManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

@Slf4j
public class AppApplicationListener implements ApplicationListener {

    @Autowired
    private ServerManager serverManager;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ContextClosedEvent) {
            log.info("================:{}", "ContextClosedEvent");
            serverManager.stop();
        }
        if (event instanceof ApplicationReadyEvent) {
            log.info("================:{}", "ApplicationReadyEvent");
            serverManager.start();
        }
    }

}
