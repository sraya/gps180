
package com.gps.run;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication(scanBasePackages = {
		"com.gps.run",
		"com.gps.api",
		"com.gps.db",
		"com.gps.engine",
		"com.gps.gate",
		"com.gps.websocket"
})
@MapperScan({"com.gps.api.modules.*.dao", "com.gps.db.dao"})
public class GpsRunApplication {

	public static void main(String[] args) {
		SpringApplication.run(GpsRunApplication.class, args);
		log.info("***启动成功***");
	}


	@Bean
	public AppApplicationListener appApplicationListener() {
		return new AppApplicationListener();
	}
}