package com.gps.run.trans;

import com.alibaba.fastjson.JSONObject;
import com.gps.api.ws.WebSocketPush;
import com.gps.common.model.CommandResult;
import com.gps.common.model.Event;
import com.gps.common.model.Position;
import org.springframework.beans.factory.annotation.Autowired;

public class EvtTransImpl {
    @Autowired
    private WebSocketPush webSocketPush;

    public void evt(String recordKey, String obj) {
        if ("pos".equals(recordKey)) {
            Position position = JSONObject.parseObject(obj, Position.class);
            webSocketPush.notifyPos(position);
        } else if ("evt".equals(recordKey)) {
            Event event = JSONObject.parseObject(obj, Event.class);
            webSocketPush.notifyEvt(event);
        } else if ("result".equals(recordKey)) {
            CommandResult result = JSONObject.parseObject(obj, CommandResult.class);
            webSocketPush.notifyResult(result);
        }
    }
}
