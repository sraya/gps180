package com.gps.run.trans;

import com.gps.api.cmd.CmdTrans;
import com.gps.common.model.Command;
import com.gps.gate.database.CommandsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CmdTransImpl implements CmdTrans {
    @Autowired
    private CommandsManager commandManager;
    @Override
    public void send(Command command) {
        commandManager.sendCommand(command);
    }
}
