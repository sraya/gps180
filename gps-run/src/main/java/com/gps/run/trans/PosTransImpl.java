package com.gps.run.trans;

import com.gps.common.model.BaseModel;
import com.gps.engine.TrackerServer;
import com.gps.gate.trans.PosTrans;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PosTransImpl implements PosTrans {

    @Autowired
    private TrackerServer server;
    @Override
    public void send(BaseModel model) {
        server.postData(model);
    }
}
